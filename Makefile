OBJS=monitoring.pdf monitoring.ps monitoring.dvi

monitoring.pdf:

all: $(OBJS)

%.dvi: %.tex
	latex -shell-escape $<
%.pdf: %.tex $(wildcard *.png)
	pdflatex -shell-escape $<
%.ps: %.pdf
	pdf2ps $< $@

clean:
	rm -f $(OBJS) *.aux *.log *.toc *.nav *.out *.snm *~


